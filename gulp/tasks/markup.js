import gulp from "gulp";

import config from "../config";

import flatten from "gulp-flatten";
import pug from "gulp-pug";

export function markup() {
  return gulp
    .src(`${config.src}/templates/pages/*.pug`)
    .pipe(pug())
    .pipe(flatten())
    .pipe(gulp.dest(config.dist));
}
