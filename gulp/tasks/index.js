/**
 * Load tasks and export globals
 */

import gulp from "gulp";

import config from "../config";

import chalk from "chalk";

import { cleanBuildFolder } from "./clean";
import { markup } from "./markup";
import { processStyles } from "./styles";
import { serve } from "./serve";
import { watch } from "./watch";
// import { bundleApp, bundleVendor } from './scripts';
// import { optimizeImages, generateSpritesheet, generateFavicons } from './images';
// import { buildSitemap } from './build';
// import { testMarkup, testScripts, testPsiMobile, testPsiDesktop } from './test';

// Content
gulp.task("markup", gulp.parallel(markup));
const markupTask = gulp.task("markup");
markupTask.description = "Generate markup from Pug.";
// gulp.task(bundleVendor);

// gulp.task(
//   'scripts',
//   gulp.parallel(bundleApp, bundleVendor)
// );
// const scriptTask = gulp.task('scripts');
// scriptTask.description = 'Bundle app scripts with browserify (watchify) and concatenate vendors.';

gulp.task("styles", gulp.parallel(processStyles));
const stylesTask = gulp.task("styles");
stylesTask.description =
  "Compile sass/stylus files with sourcemaps + autoprefixer.";

// gulp.task(
//   'images',
//   gulp.parallel(optimizeImages, generateSpritesheet, generateFavicons)
// );
// const imagesTask = gulp.task('images');
// imagesTask.description =
//   'Optimize new images and cache them, create a spritesheet and generate favicons/metas.';

// Utils
gulp.task(serve);
gulp.task(watch);
gulp.task("clean", gulp.parallel(cleanBuildFolder));
const cleanTask = gulp.task("clean");
cleanTask.description = "Clean build folder.";

// Build
// gulp.task(
//   'build',
//   gulp.parallel('scripts', 'styles', buildSitemap)
// );
// const buildTask = gulp.task('build');
// buildTask.description = 'Build scripts and styles with minification tasks.';

// Tests
// gulp.task(
//   'testPsi',
//   gulp.parallel(testPsiMobile, testPsiDesktop)
// );
// const testPsiTask = gulp.task('testPsi');
// testPsiTask.description = 'PageSpeed Insights reportings.';

// gulp.task(testScripts);
// gulp.task(testMarkup);

// Default task
if (config.args.env === "dev") {
  gulp.task(
    "default",
    gulp.series(
      "clean",
      gulp.parallel(markup, "styles"),
      gulp.parallel(serve, watch)
    )
  );
} else if (config.args.env === "prod") {
  gulp.task("default", gulp.series("clean", gulp.parallel(markup, "styles")));
} else {
  console.log(chalk.red("--env flag should be either dev or prod"));
  process.exit(1);
}

const defaultTask = gulp.task("default");
defaultTask.description = "Launch dev or prod default task.";
