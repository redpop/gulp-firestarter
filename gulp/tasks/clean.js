import gulp from "gulp";

import config from "../config";

import del from "del";

gulp.task("clean", gulp.parallel(cleanBuildFolder));
const cleanTask = gulp.task("clean");
cleanTask.description = "Clean build folder.";

export function cleanBuildFolder(done) {
  return del(
    [`${config.dist}/**`, `${config.test}/output/**`, `!${config.dist}`],
    done
  );
}
