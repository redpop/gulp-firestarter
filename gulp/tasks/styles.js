import gulp from "gulp";

import config from "../config";

import autoprefixer from "autoprefixer";
import csswring from "csswring";
import header from "gulp-header";
import gutil from "gulp-util";
import mqpacker from "css-mqpacker";
import postcss from "gulp-postcss";
import rename from "gulp-rename";
import sourcemaps from "gulp-sourcemaps";

import handleErrors from "../utils/handleErrors";
import { server } from "./serve";

let preprocessor;
let processors;
const envDev = config.args.env === "dev";

// Processors
if (envDev) {
  processors = [autoprefixer()];
} else {
  processors = [
    autoprefixer(),
    mqpacker,
    csswring({
      preserveHacks: true,
      removeAllComments: true
    })
  ];
}

// Preprocessor
let sassGlob;
let lessGlob;

switch (config.extensions.styles) {
  case "scss": {
    preprocessor = require("gulp-sass");
    sassGlob = require("gulp-sass-glob");
    break;
  }

  case "less": {
    preprocessor = require("gulp-less");
    lessGlob = require("less-plugin-glob");
    break;
  }

  case "styl": {
    preprocessor = require("gulp-stylus");
    break;
  }

  case "css": {
    break;
  }

  default:
    console.log("Wrong css extension in package.json");
    break;
}

function getStylesStream(extension) {
  switch (extension) {
    case "scss":
      return gulp
        .src(`${config.src}/styles/app.scss`)
        .pipe(sassGlob())
        .pipe(sourcemaps.init())
        .pipe(
          preprocessor({
            includePaths: "./node_modules"
          })
        );

    case "less":
      return gulp
        .src(`${config.src}/styles/main.less`)
        .pipe(sourcemaps.init())
        .pipe(
          preprocessor({
            plugins: [lessGlob]
          })
        );

    case "styl":
      return gulp
        .src(`${config.src}/styles/app.styl`)
        .pipe(sourcemaps.init())
        .pipe(
          preprocessor({
            "include css": true
          })
        );

    default:
      return gulp.src(`${config.src}/styles/**/*.css`);
  }
}

export function processStyles() {
  return getStylesStream(config.extensions.styles)
    .on("error", handleErrors)
    .pipe(postcss(processors))
    .pipe(envDev ? sourcemaps.write() : gutil.noop())
    .pipe(envDev ? gutil.noop() : header(config.banner))
    .pipe(
      envDev
        ? gutil.noop()
        : rename({
            suffix: ""
          })
    )
    .pipe(gulp.dest(`${config.dist}/styles`))
    .pipe(server.stream());
}
